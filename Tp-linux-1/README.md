# TP 1 : Are you dead yet ?

---

**🌞 Plusieurs façons différentes de péter la machine :**

## Premiere façon :
```bash
dreasy@dreasy:~$ sudo rm -rf /
``` 
Ici je supprime tout depuis la racine

## Deuxième façon :
```bash
dreasy@dreasy:~$ sudo rm /sbin/init
```
Là je supprime le premier processus lancé par linux
    
## Troisième façon :
```bash
dreasy@dreasy:~$ sudo chmod 400 /lib/systemd/systemd
```
Ici je modifie le système de manière à ce qu'on puisse simplement lire sans exécuter les fichiers


## Quatrième façon :
```bash
dreasy@dreasy:~$ sudo rm -rf home
```
Ici je supprime le fichier de session. Cela devient impossible de se connecter

## Cinquième façon :
```bash
dreasy@dreasy:~$ sudo rm -rf --no-preserve-root find / | grep kernel
```
Pour finir je supprime les kernel de linux
