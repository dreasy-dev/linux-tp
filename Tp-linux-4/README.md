# TP4 : Une distribution orientée serveur

# Sommaire

- [TP4 : Une distribution orientée serveur](#tp4--une-distribution-orientée-serveur)
- [Sommaire](#sommaire)
- [I. Install de Rocky Linux](#i-install-de-rocky-linux)
- [II. Checklist](#ii-checklist)
- [III. Mettre en place un service](#iii-mettre-en-place-un-service)
  - [1. Intro NGINX](#1-intro-nginx)
  - [2. Install](#2-install)
  - [3. Analyse](#3-analyse)
  - [4. Visite du service web](#4-visite-du-service-web)
  - [5. Modif de la conf du serveur web](#5-modif-de-la-conf-du-serveur-web)

# I. Install de Rocky Linux


Rocky Linux est une distribution orientée robustesse, sécurité et stabilité. Elle est idéale pour des serveurs.  
C'est une distribution de la famille RedHat.

> xubuntu ou juste Ubuntu, c'est  de la famille Debian.

Vous allez réaliser une install de RockyLinux. Les paramètres à changer lors de l'install :

- langue : anglais
- clavier : azerty (enfin, en accord avec votre clavier quoi)
- allumer la carte NAT
- créer un utilisateur administrateur et lui donner un mot de passe
- donner un mot de passer à root

Maintenant, petit détails de config : allumez la VM, loggez vous puis exécutez :

```bash
# On désactive un truc qui s'appelle SELinux sur lequel je vais pas m'étendre
$ sudo sed -i 's/enforcing/permissive/g' /etc/selinux/config

# Profitez en aussi pour mettre la machine à jour.
# Dans les systèmes RedHat, le gestionnaire de paquets c'est dnf, pas apt
$ sudo dnf update -y
```

Eteginez maintenant la VM, elle est prête à être clonée à chaque fois qu'on a besoin d'une machine Rocky Linux.

# II. Checklist



A chaque fois que vous créerez une machine Rocky Linux, vous vérifierez les éléments de la checklist qui suit.

Pour cette première fois, vous me rendrez dans le compte-rendu les étapes que vous avez réalisé pour mener ça à bien. Dans les futurs TPs, la checklist devra simplement être réalisée à chaque fois.

**Créez donc une nouvelle machine** (clonez celle qu'on vient d'install). On l'appellera `node1.tp4.linux`.

---

➜ **Configuration IP statique**

Jusqu'à maintenant on laissait notre machine récupérer une IP automatiquement. Pour des clients, c'est ce qui se passe dans la vie réelle (genre ton smartphone connecté à ta box, il récup une IP automatiquement).

**Pour un serveur, c'est différent : on veut que son IP soit prévisible, donc fixe. On la définit donc à la main.**

Dans Rockly Linux, on définit la configuration des cartes réseau dans le dossier `/etc/sysconfig/network-scripts/`. Il existe là-bas (on peut le créer s'il n'existe pas), un fichier par interface réseau.

Par exemple, pour l'interface `enp0s8`, le fichier de configuration se nommera `ifcfg-enp0s8`, au chemin`/etc/sysconfig/network-scripts/ifcfg-enp0s8`.

Le contenu que vous devez utiliser :

```
NAME=enp0s8          # nom de l'interface
DEVICE=enp0s8        # nom de l'interface
BOOTPROTO=static     # définition statique de l'IP (par opposition à DHCP)
ONBOOT=yes           # la carte s'allumera automatiquement au boot de la machine
IPADDR=<IP_CHOISIE>  # adresse IP choisie
NETMASK=<MASK>       # masque choisi
```

A chaque fois que vous modifiez un fichier de configuration d'interface, il faudra exécuter :

```bash
# on indique au système que les fichiers ont été modifiés
$ sudo nmcli con reload

# on allume/reload l'interface avec la nouvelle conf
$ sudo nmcli con up <INTERFACE>
# par exemple
$ sudo nmcli con up enp0s8
```

🌞 **Choisissez et définissez une IP à la VM**

- vous allez devoir configurer l'interface host-only
- ce sera nécessaire pour pouvoir SSH dans la VM et donc écrire le compte-rendu
- je veux, dans le compte-rendu, le contenu de votre fichier de conf, et le résultat d'un `ip a` pour me prouver que les changements on pris effet

         3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000 link/ether 08:00:27:e7:c2:a1 brd                                                                inet 10.250.1.45/24 brd 10.250.1.255 

---

➜ **Connexion SSH fonctionnelle**

Vous pouvez vous connecter en SSH à la VM. Cela implique :

- la VM a une carte réseau avec une IP locale
- votre PC peut joindre cette IP
- la VM a un serveur SSH qui est accessible derrière l'un des ports réseau

🌞 **Vous me prouverez que :**

- le service ssh est actif sur la VM
  - avec une commande `systemctl status ...`

            dreasy@localhost~]$
            ● sshd.service - OpenSSH server daemon                                                                                                   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)                                                         Active: active (running) since Tue 2021-11-23 16:20:50 CET; 20min ago                                                                   Docs: man:sshd(8)                                                                                                                           man:sshd_config(5) 

- vous pouvez vous connecter à la VM, grâce à un échange de clés


#### ➜ [**Voir le cadenas client**](./files/ssh_linux.pub)

#### ➜ [**Voir les authorized_keys server**](./files/authorized_keys)
> Pour me prouver que vous pouvez vous connecter avec un échange de clé il faut me montrer, dans l'idéal : la clé publique (le cadenas) sur votre PC (un `cat` du fichier), un `cat` du fichier `authorized_keys` concerné sur la VM, et une connexion sans aucun mot de passe demandé.

---

➜ **Accès internet**

Par l'expression commune et barbare "avoir un accès internet" on entend deux choses généralement :

- *ahem* avoir un accès internet
  - c'est à dire être en mesure de ping des IP publiques
- avoir de la résolution de noms
  - la machine doit pouvoir traduire un nom comme `google.com` vers l'IP associée

🌞 **Prouvez que vous avez un accès internet**

- avec une commande `ping`

> On utilise souvent un `ping` vers une adresse IP publique connue pour tester l'accès internet.  
Vous verrez souvent `8.8.8.8` c'est l'adresse d'un serveur Google. On part du principe que Google sera bien le dernier à quitter Internet, donc on teste l'accès internet en le pingant.  
Si vous n'aimez pas Google comme moi, vous pouvez `ping 1.1.1.1`, c'est les serveurs de CloudFlare.

```bash
 [dreasy@localhost .ssh]$ ping 8.8.8.8                                                                                   PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.                                                                            64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=23.2 ms                                                                  64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=22.9 ms                                                                  64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=24.0 ms                                                                  64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=24.8 ms                                                                  64 bytes from 8.8.8.8: icmp_seq=5 ttl=113 time=25.0 ms                                                                  64 bytes from 8.8.8.8: icmp_seq=6 ttl=113 time=25.2 ms                                                                  64 bytes from 8.8.8.8: icmp_seq=7 ttl=113 time=26.9 ms                                                                  64 bytes from 8.8.8.8: icmp_seq=8 ttl=113 time=24.4 ms                                                                                                                                                                                       --- 8.8.8.8 ping statistics ---                                                                                         8 packets transmitted, 8 received, 0% packet loss, time 7014ms                                                          rtt min/avg/max/mdev = 22.944/24.563/26.862/1.172 ms                                                                    [dreasy@localhost .ssh]$ ping 1.1.1.1                                                                                   PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.                                                                            64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=24.10 ms                                                                  64 bytes from 1.1.1.1: icmp_seq=2 ttl=54 time=26.5 ms                                                                   64 bytes from 1.1.1.1: icmp_seq=3 ttl=54 time=27.2 ms                                                                   64 bytes from 1.1.1.1: icmp_seq=4 ttl=54 time=27.0 ms                                                                   64 bytes from 1.1.1.1: icmp_seq=5 ttl=54 time=25.0 ms                                                                   64 bytes from 1.1.1.1: icmp_seq=6 ttl=54 time=26.6 ms                                                                                                                                                                                        --- 1.1.1.1 ping statistics ---                                                                                         6 packets transmitted, 6 received, 0% packet loss, time 5054ms                                                          rtt min/avg/max/mdev = 24.978/26.231/27.226/0.901 ms 
 ```

🌞 **Prouvez que vous avez de la résolution de nom**

Un petit `ping` vers un nom de domaine, celui que vous voulez :)
```bash
[dreasy@localhost .ssh]$ ping ynov.com                                                                                  PING ynov.com (92.243.16.143) 56(84) bytes of data.                                                                     64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=51 time=22.4 ms                                   64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=51 time=28.2 ms                                   64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=51 time=48.6 ms                                   64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=4 ttl=51 time=23.9 ms                                                                                                                                                         --- ynov.com ping statistics ---                                                                                        4 packets transmitted, 4 received, 0% packet loss, time 3033ms                                                          rtt min/avg/max/mdev = 22.386/30.763/48.579/10.509 ms 
```
---

➜ **Nommage de la machine**

Votre chambre vous la rangez na ? Au moins de temps en temps ? Là c'est pareil : on range les machines. Et ça commence par leur donner un nom.

Pour donner un nom à une machine il faut exécuter deux actions :

- changer son nom immédiatement, mais le changement sera perdu au reboot
- écrire son nom dans un fichier, pour que le fichier soit lu au boot et que le changement persiste

En commande ça donne :

```bash
# Pour la session
$ sudo hostname <NOM>

# Persistant après les reboots
$ sudo nano /etc/hostname # remplacer le contenu du fichier par le NOM
```

🌞 **Définissez `node1.tp4.linux` comme nom à la machine**

- montrez moi le contenu du fichier `/etc/hostname`

            [dreasy@dreasy ~]$ cat /etc/hostname                                                               
            node1.tp4.linux 
            
- tapez la commande `hostname` (sans argument ni option) pour afficher votre hostname actuel

            [dreasy@dreasy ~]$ sudo hostname                                                                                    
            node1.tp4.linux   


Apres un systemctl reboot (soit un reboot machine) j'ai mtn sur le terminal:

            Dreasy@node1

> Vous verrez aussi votre hostname dans le prompt du terminal, en plus de l'utilisateur avec lequel vous êtes co : `it4@node1:~$`.  
C'est de l'anglais en fait hein, le "@" se dit "at". Donc en l'occurence "it4 at node1".

# III. Mettre en place un service

On va installer un service de test pour voir la procédure récurrente d'installation et configuration d'un nouveau service.  
C'est un peu la routine de l'administrateur système. My job heh.

Toujours les mêmes opérations :

- **install du service**
  - souvent via des paquets
- puis **conf du service**
  - dans des fichiers texte de configuration
- puis si c'est un service qui utilise le réseau, il faudra **ouvrir** un port dans le pare-feu
  - hé ui, fini les distribs comme xubuntu où c'est open-bar
  - Rocky Linux bloque la plupart du trafic qui essaie d'entrer sur la machine
- puis **lancement du service**
  - ptite commande `systemctl start` généralement
- puis **enjoy**
  - consommation du service par des clients

On va setup un serveur web. Cas d'école un peu, on va s'en servir pour approfondir encore notre maîtrise des services.

## 1. Intro NGINX


NGINX (prononcé "engine-X") est un serveur web. C'est un outil de référence aujourd'hui, il est réputé pour ses performances et sa robustesse.

Ici on va pas DU TOUT s'attarder sur la partie dév web étou, une simple page HTML fera l'affaire.

Une fois le serveur web installé, on récupère :

- **un service**
  - un service c'est un processus
  - il y a donc un binaire, une application qui fait serveur web
  - qui dit processus, dit que quelqu'un, un utilisateur lance ce processus
  - c'est l'utilisateur qu'on voit lister dans la sortie de `ps -ef`
- **des fichiers de conf**
  - comme d'hab c'est dans `/etc/` la conf
  - comme d'hab c'est bien rangé, donc la conf de NGINX c'est dans `/etc/nginx/`
  - question de simplicité en terme de nommage, le fichier de conf principal c'est `/etc/nginx/nginx.conf`
  - la conf, ça appartient à l'utilisateur `root`
- **une racine web**
  - c'est un dossier dans lequel le site est stocké
  - c'est à dire là où se trouvent tous les fichiers PHP, HTML, CSS, JS, etc du site
  - ce dossier et tout son contenu doivent appartenir à l'utilisateur qui lance le service
- **des logs**
  - tant que le service a pas trop tourné c'est empty
  - comme d'hab c'est `/var/log/`
  - comme d'hab c'est bien rangé donc c'est dans `/var/log/nginx/`
  - comme d'hab on peut aussi consulter certains logs avec `sudo journalctl -xe -u nginx`

## 2. Install

🌞 **Installez NGINX en vous référant à des docs online**

Vous devez comprendre toutes les commandes que vous tapez. En deux trois commandes c'est plié l'install.

```bash
dreasy@node1:$ sudo dnf install nginx
#install nginx
dreasy@node1:$ sudo systemctl start nginx
#start le service nginx
dreasy@node1:$ sudo systemctl enable nginx
#activer nginx en tant que service
dreasy@node1:$ nginx -v
#check la version actuel 
dreasy@node1:$ sudo firewall-cmd --permanent --zone=public --add-service=http
#activer le firewall pour le http
dreasy@node1:$ sudo firewall-cmd --reload
#reload le firewall pour mettre la conf a jour 
```
## 3. Analyse

Avant de config étou, on va lancer à l'aveugle et inspecter ce qu'il se passe.

Commencez donc par démarrer le service NGINX :

```bash
$ sudo systemctl start nginx
$ sudo systemctl status nginx
```

🌞 **Analysez le service NGINX**

- avec une commande `ps`, déterminer sous quel utilisateur tourne le processus du service NGINX

            
    
        root        4100       1  0 17:26 ?        00:00:00 nginx: master process /usr/sbin/nginx

- avec une commande `ss`, déterminer derrière quel port écoute actuellement le serveur web

            dreasy@node1 :$ sudo ss -lnpt
            LISTEN                0                     128                                         0.0.0.0:80                                        0.0.0.0:*                    users:(("nginx",pid=4245,fd=8),("nginx",pid=4244,fd=8),("nginx",pid=4243,fd=8),("nginx",pid=4242,fd=8),("nginx",pid=4100,fd=8))                                                      

- en regardant la conf, déterminer dans quel dossier se trouve la racine web

            /usr/share/nginx/html

- inspectez les fichiers de la racine web, et vérifier qu'ils sont bien accessibles en lecture par l'utilisateur qui lance le processus

            [dreasy@node1 html]$ cat index.html  
                                                                                               <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">                                                                                                                                               <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">                                                                 <head>                                                                                                                    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>                                                       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
            [...]

## 4. Visite du service web

Et ça serait bien d'accéder au service non ? Bon je vous laisse pas dans le mur : spoiler alert, le service est actif, mais le firewall de Rocky bloque l'accès au service, on va donc devoir le configurer.

Il existe [un mémo dédié au réseau au réseau sous Rocky](../../cours/memos/rocky_network.md), vous trouverez le nécessaire là-bas pour le firewall.

🌞 **Configurez le firewall pour autoriser le trafic vers le service NGINX** (c'est du TCP ;) )

```bash
[dreasy@node1 /]$ sudo firewall-cmd --list-all                                                                          public (active)                                                                                                           target: default                                                                                                         icmp-block-inversion: no                                                                                                interfaces: enp0s3 enp0s8                                                                                               sources:                                                                                                                services: cockpit dhcpv6-client http https ssh                                                                          ports: 80/tcp                                                                                                           protocols:                                                                                                              forward: no                                                                                                             masquerade: no                                                                                                          forward-ports:                                                                                                          source-ports:                                                                                                           icmp-blocks:                                                                                                            rich rules: 
```

🌞 **Tester le bon fonctionnement du service**

- avec votre navigateur sur VOTRE PC
  - ouvrez le navigateur vers l'URL : `http://<IP_VM>:<PORT>`

            http://10.250.1.45:80
- vous pouvez aussi effectuer des requêtes HTTP depuis le terminal, plutôt qu'avec un navigateur
  - ça se fait avec la commande `curl`
  - et c'est ça que je veux dans le compte-rendu, pas de screen du navigateur :)

    #### ➜ [**Voir le Curl**](./files/curl.bash)

## 5. Modif de la conf du serveur web

🌞 **Changer le port d'écoute**

- une simple ligne à modifier, vous me la montrerez dans le compte rendu
  - faites écouter NGINX sur le port 8080
- redémarrer le service pour que le changement prenne effet
  - `sudo systemctl restart nginx`
  - vérifiez qu'il tourne toujours avec un ptit `systemctl status nginx`
- prouvez-moi que le changement a pris effet avec une commande `ss`

```bash
dreasy.node1:$ sudo ss -lnpt 
LISTEN          0               128                               [::]:8080                           [::]:*             users:(("nginx",pid=4582,fd=13),("nginx",pid=4581,fd=13),("nginx",pid=4580,fd=13),("nginx",pid=4579,fd=13),("nginx",pid=4100,fd=13)) 
```

- n'oubliez pas de fermer l'ancier port dans le firewall, et d'ouvrir le nouveau
- prouvez avec une commande `curl` sur votre machine que vous pouvez désormais visiter le port 8080

#### ➜ [**Voir le Curl (modifié)**](./files/curl2.bash)

---

🌞 **Changer l'utilisateur qui lance le service**

- pour ça, vous créerez vous-même un nouvel utilisateur sur le système : `web`
  - référez-vous au [mémo des commandes](../../cours/memos/commandes.md) pour la création d'utilisateur
  - l'utilisateur devra avoir un mot de passe, et un homedir défini explicitement à `/home/web`
- un peu de conf à modifier dans le fichier de conf de NGINX pour définir le nouvel utilisateur en tant que celui qui lance le service
  - vous me montrerez la conf effectuée dans le compte-rendu
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- vous prouverez avec une commande `ps` que le service tourne bien sous ce nouveau utilisateur

```bash
dreasy@node1:$ ps -ef 
web         1872    1871  0 19:31 ?        00:00:00 nginx: worker process                                                             web         1873    1871  0 19:31 ?        00:00:00 nginx: worker process                                                             web         1874    1871  0 19:31 ?        00:00:00 nginx: worker process                                                             web         1875    1871  0 19:31 ?        00:00:00 nginx: worker process                                                             dreasy      1883    1519  0 19:31 pts/0    00:00:00 ps -ef   
```
---

🌞 **Changer l'emplacement de la racine Web**

- vous créerez un nouveau dossier : `/var/www/super_site_web`
  - avec un fichier  `/var/www/super_site_web/index.html` qui contient deux trois lignes de HTML, peu importe, un bon `<h1>toto</h1>` des familles, ça fera l'affaire
  - le dossier et tout son contenu doivent appartenir à `web`
- configurez NGINX pour qu'il utilise cette nouvelle racine web
  - vous me montrerez la conf effectuée dans le compte-rendu
  #### ➜ [**Voir le fichier de conf**](./files/conf.md)
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- prouvez avec un `curl` depuis votre hôte que vous accédez bien au nouveau site

```bash
PS C:\Users\Dreasy> curl http://10.250.1.45:8080                                                                                                                                                                                                                                                                                                StatusCode        : 200                                                                                         StatusDescription : OK                                                                                          Content           : <h1>site du turfu</h1>                                                                                                                                                                                      RawContent        : HTTP/1.1 200 OK                                                                                                 Connection: keep-alive                                                                                          Accept-Ranges: bytes                                                                                            Content-Length: 23                                                                                              Content-Type: text/html                                                                                         Date: Wed, 24 Nov 2021 18:42:21 GMT                                                                             ETag: "619e85ce-17"                                                                                             Last-Modified: Wed, 24 Nov 2021 18...                                                       Forms             : {}                                                                                          Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes], [Content-Length, 23], [Content-Type,                         text/html]...}                                                                              Images            : {}                                                                                          InputFields       : {}                                                                                          Links             : {}                                                                                          ParsedHtml        : mshtml.HTMLDocumentClass                                                                    RawContentLength  : 23                                                                         ```                                          