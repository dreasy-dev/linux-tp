# TP5 : P'tit cloud perso

Dans ce TP on va monter un ptit serveur cloud nous-mêmes. On parle ici d'un serveur de partage de fichiers, accessible depuis une jolie interface Web.

Un peu comme un DropBox ou Google Drive. Mais en mieux. Et à nous. :3

![There is no cloud](./pics/there_is_no_cloud.jpg)

La solution qu'on va utiliser c'est [NextCloud](https://nextcloud.com/). C'est libre, open-source, joli et fonctionnel :)

NextCloud, pour fonctionner, a besoin de :

- un serveur web
  - pour varier on va utiliser Apache plutôt que NGINX
  - NextCloud est codé en PHP, il faudra donc ajouter PHP à la machine
  - NextCloud, c'est un ensemble de fichier PHP, JS, HTML, etc. comme n'importe quel site web
    - il faudra donc déplacer ces fichiers dans une racine web servie par Apache
- une base de données
  - on va utiliser MariaDB
    - c'est la base de données de type MySQL qui est disponible par défaut sur Rocky
  - on aura besoin de taper quelques commandes SQL mais rien de bien méchant

Question de bonne pratique : on installe un unique service sur une machine donnée.  
Ainsi, le serveur Web sera sur une machine, la base de données sur une autre.

Cela permet de simplifier la gestion, d'éviter que tout soit down si un serveur donné est down, etc.

> Les instructions données dans le TP sont grandement inspirées de [la doc officielle de Rocky pour installer NextCloud](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/).

# Sommaire

- [0. Prérequis](#0-prérequis)
- [I. Setup DB](./db.md)
- [II. Setup Web](./web.md)

# 0. Prérequis

Pour ce TP, on va donc avoir besoin de deux machines virtuelles 🖥️ :

| Machine         | IP             | Service              |
|-----------------|----------------|----------------------|
| `web.tp5.linux` | `10.5.1.11/24` | Serveur Web : Apache |
| `db.tp5.linux`  | `10.5.1.12/24` | Serveur de base de données : MariaDB |

Vous déroulerez la **📝checklist📝** suivante, sur les deux machines :

- [x] la VM a un accès internet
  - `ping 1.1.1.1` fonctionnel
  - résolution de nom fonctionnelle : `ping ynov.com` fonctionnel
- [x] SELinux est désactivé
  - la commande `sestatus` doit retourner : `Current Mode: permissive`
- [x] vous avez une connexion SSH fonctionnelle vers la VM
  - avec un échange de clé
- [x] la machine est nommée
  - fichier `/etc/hostname`
  - commande `hostname`

> La checklist ne doit pas figurer dans le rendu, elle doit simplement être effectuée sur toutes les machines. Tout manquement à la checklist vous vaudra des points en moins.

Vous pourrez ensuite dérouler la mise en place de NextCloud :

- [I. Setup DB](./db.md)
- [II. Setup Web](./web.md)
