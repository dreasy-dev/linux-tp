PS C:\Users\Dreasy> curl 10.5.1.11
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.   
If you can read this page, it means that the software it working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenance.
If you would like the let the administrators of this website know that you've seen this page instead of the page
you've expected, you should send them an email. In general, mail sent to the name "webmaster" and directed to the
website's domain should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"
Note:
The Rocky Linux distribution is a stable and reproduceable platform based on the sources of Red Hat Enterprise Linux
(RHEL). With this in mind, please understand that:
Neither the Rocky Linux Project nor the Rocky Enterprise Software Foundation have anything to do with this website or   its content.
The Rocky Linux Project nor the RESF have "hacked" this webserver: This test page is included with the distribution.    
For more information about Rocky Linux, please visit the Rocky Linux website.                                           
I am the admin, what do I do?
You may now add content to the webroot directory for your software.
For systems using the Apache Webserver: You can add content to the directory /var/www/html/. Until you do so, people    
visiting your website will see this page. If you would like this page to not be shown, follow the instructions in:      /etc/httpd/conf.d/welcome.conf.  
Apache™ is a registered trademark of the Apache Software Foundation in the United States and/or other countries.