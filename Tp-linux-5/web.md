# II. Setup Web

Comme annoncé dans l'intro, on va se servir d'Apache dans le rôle de serveur Web dans ce TP5. Histoire de varier les plaisirs è_é

![Linux is a tipi](./pics/linux_is_a_tipi.jpg)

## Sommaire

- [II. Setup Web](#ii-setup-web)
  - [Sommaire](#sommaire)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
  - [3. Install NextCloud](#3-install-nextcloud)
  - [4. Test](#4-test)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp5.linux`**

- le paquet qui contient Apache s'appelle `httpd`
```bash
sudo dnf install httpd
```
- le service aussi s'appelle `httpd`
```bash
[dreasy@web ~]$ systemctl status httpd 
 ● httpd.service - The Apache HTTP Server
Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled) 
 Active: active (running) since Fri 2021-11-26 18:07:05 CET; 16s ago 
  Docs: man:httpd.service(8)
  Main PID: 923 (httpd)
```

---

🌞 **Analyse du service Apache**

- lancez le service `httpd` et activez le au démarrage
- isolez les processus liés au service `httpd`
- déterminez sur quel port écoute Apache par défaut
```bash
 users:(("httpd",pid=1040,fd=4),("httpd",pid=1039,fd=4),("httpd",pid=1037,fd=4),("httpd",pid=923,fd=4)) 
 LISTEN          0               128                               [::]:22                             [::]:*           
 ```
- déterminez sous quel utilisateur sont lancés les processus Apache
```bash
apache      1036     923  0 18:07 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND                                        apache      1037     923  0 18:07 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND                                        apache      1039     923  0 18:07 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND                                        apache      1040     923  0 18:07 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
 ```
 tous lancé par un nouveau user "apache"


---

🌞 **Un premier test**

- ouvrez le port d'Apache dans le firewall
- testez, depuis votre PC, que vous pouvez accéder à la page d'accueil par défaut d'Apache
  - avec une commande `curl`
  - avec votre navigateur Web

  [***Curl***](./curl.cu)


### B. PHP

NextCloud a besoin d'une version bien spécifique de PHP.  
Suivez **scrupuleusement** les instructions qui suivent pour l'installer.

🌞 **Installer PHP**

```bash
# ajout des dépôts EPEL
$ sudo dnf install epel-release
$ sudo dnf update
# ajout des dépôts REMI
$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
$ dnf module enable php:remi-7.4

# install de PHP et de toutes les libs PHP requises par NextCloud
$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
```

## 2. Conf Apache

➜ Le fichier de conf utilisé par Apache est `/etc/httpd/conf/httpd.conf`.  
Il y en a plein d'autres : ils sont inclus par le premier.

➜ Dans Apache, il existe la notion de *VirtualHost*. On définit des *VirtualHost* dans les fichiers de conf d'Apache.  
On crée un *VirtualHost* pour chaque application web qu'héberge Apache.

> "Application Web" c'est le terme de hipster pour désigner un site web. Disons qu'aujourd'hui les sites peuvent faire tellement de trucs qu'on appelle plutôt ça une "application" à part entière. Une application web donc.

➜ Dans le dossier `/etc/httpd/` se trouve un dossier `conf.d`.  
Des dossiers qui se terminent par `.d`, vous en rencontrerez plein, ce sont des dossiers de *drop-in*.  
Plutôt que d'écrire 40000 lignes dans un seul fichier de conf, on l'éclate en plusieurs fichiers la conf.  
C'est + lisible et + facilement maintenable.

Les dossiers de *drop-in* servent à accueillir ces fichiers de conf additionels.  
Le fichier de conf principal a une ligne qui inclut tous les fichiers de conf contenus dans le dossier de *drop-in*.

---

🌞 **Analyser la conf Apache**

- mettez en évidence, dans le fichier de conf principal d'Apache, la ligne qui inclut tout ce qu'il y a dans le dossier de *drop-in*

```bash
# Load config files in the "/etc/httpd/conf.d" directory, if any.
# IncludeOptional conf.d/*.conf  
```

🌞 **Créer un VirtualHost qui accueillera NextCloud**

- créez un nouveau fichier dans le dossier de *drop-in*
  - attention, il devra être correctement nommé (l'extension) pour être inclus par le fichier de conf principal
- ce fichier devra avoir le contenu suivant :

```apache
<VirtualHost *:80>
  DocumentRoot /var/www/nextcloud/html/  # on précise ici le dossier qui contiendra le site : la racine Web
  ServerName  web.tp5.linux  # ici le nom qui sera utilisé pour accéder à l'application

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

> N'oubliez pas de redémarrer le service à chaque changement de la configuration, pour que les changements prennent effet.
```bash 
PS C:\Users\Dreasy> ssh dreasy@10.5.1.11                                                                                Activate the web console with: systemctl enable --now cockpit.socket                                                                                                                                                                            Last login: Fri Nov 26 19:46:36 2021 from 10.5.1.1                                                                      Activate the web console with: systemctl enable --now cockpit.socket                                                                                                                                                                            Last login: Fri Nov 26 19:46:36 2021 from 10.5.1.1                                                                      [dreasy@web ~]$ systemctl status httpd                                                                                  ● httpd.service - The Apache HTTP Server                                                                                   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)                               Drop-In: /usr/lib/systemd/system/httpd.service.d                                                                                 └─php74-php-fpm.conf                                                                                            Active: active (running) since Fri 2021-11-26 19:50:28 CET; 44s ago                                                       Docs: man:httpd.service(8)                                                                                          Main PID: 912 (httpd)
```
🌞 **Configurer la racine web**

- la racine Web, on l'a configurée dans Apache pour être le dossier `/var/www/nextcloud/html/`
- creéz ce dossier
- faites appartenir le dossier et son contenu à l'utilisateur qui lance Apache (commande `chown`, voir le [mémo commandes](../../cours/memos/commandes.md))

```bash
dreasy.web:$ chown apache /nextcloud/html 
```

> Jusqu'à la fin du TP, tout le contenu de ce dossier doit appartenir à l'utilisateur qui lance Apache. C'est strictement nécessaire pour qu'Apache puisse lire le contenu, et le servir aux clients.

🌞 **Configurer PHP**

- dans l'install de NextCloud, PHP a besoin de conaître votre timezone (fuseau horaire)
- pour récupérer la timezone actuelle de la machine, utilisez la commande `timedatectl` (sans argument)
- modifiez le fichier `/etc/opt/remi/php74/php.ini` :
  - changez la ligne `;date.timezone =`
  - par `date.timezone = "<VOTRE_TIMEZONE>"`
  - par exemple `date.timezone = "Europe/Paris"`

  ```bash
  [Date]                                                                                               ; Defines the default timezone used by the date functions 
  ; http://php.net/date.timezone
  ;date.timezone = "Europe/Paris"    
  ```

## 3. Install NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

```bash
# Petit tips : la commande cd sans argument permet de retourner dans votre homedir
$ cd

# La commande curl -SLO permet de rapidement télécharger un fichier, en HTTP/HTTPS, dans le dossier courant
$ curl -SLO -k https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip

$ ls
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

- extraire le contenu de NextCloud (beh ui on a récup un `.zip`)
- déplacer tout le contenu dans la racine Web
```bash
[dreasy@web ~]$ sudo mv nextcloud /var/www/nextcloud/html/ 
```
  - n'oubliez pas de gérer les permissions de tous les fichiers déplacés ;)
- supprimer l'archive
```bash
[dreasy@web ~]$ ls                                                                                                      nextcloud-21.0.1.zip                                                                                                    [dreasy@web ~]$ rm -rf nextcloud-21.0.1.zip  
```

## 4. Test

Bah on arrive sur la fin !

Si on résume :

- **un serveur de base de données : `db.tp5.linux`**
  - MariaDB installé et fonctionnel
  - firewall configuré
  - une base de données et un user pour NextCloud ont été créés dans MariaDB
- **un serveur Web : `web.tp5.linux`**
  - Apache installé et fonctionnel
  - firewall configuré
  - un VirtualHost qui pointe vers la racine `/var/www/nextcloud/html/`
  - NextCloud installé dans le dossier `/var/www/nextcloud/html/`

**Looks like we're ready.**

---

**Ouuu presque. Pour que NextCloud fonctionne correctement, il faut y accéder en utilisant un nom, et pas une IP.**  
On va donc devoir faire en sorte que, depuis votre PC, vous puissiez écrire `http://web.tp5.linux` plutôt que `http://10.5.1.11`.

➜ Pour faire ça, on va utiliser **le fichier `hosts`**. C'est un fichier présents sur toutes les machines, sur tous les OS.  
Il sert à définir, localement, une correspondance entre une IP et un ou plusieurs noms.  

C'est arbitraire, on fait ce qu'on veut.  
Si on veut que `www.ynov.com` pointe vers le site de notre VM, ou vers n'importe quelle autre IP, on peut.  
ON PEUT TOUT FAIRE JE TE DIS.  
Ce sera évidemment valable uniquement sur la machine où se trouve le fichier.

Emplacement du fichier `hosts` :

- MacOS/Linux : `/etc/hosts`
- Windows : `c:\windows\system32\drivers\etc\hosts`

---

🌞 **Modifiez le fichier `hosts` de votre PC**

- ajoutez la ligne : `10.5.1.11 web.tp5.linux`

    check

 **Tester l'accès à NextCloud et finaliser son install'**

- ouvrez votre navigateur Web sur votre PC
- rdv à l'URL `http://web.tp5.linux`
- vous devriez avoir la page d'accueil de NextCloud
- ici deux choses :
  - les deux champs en haut pour créer un user admin au sein de NextCloud
  - le bouton "Configure the database" en bas
    - sélectionnez "MySQL/MariaDB"
    - entrez les infos pour que NextCloud sache comment se connecter à votre serveur de base de données
    - c'est les infos avec lesquelles vous avez validé à la main le bon fonctionnement de MariaDB (c'était avec la commande `mysql`)

    ![Well Done](./pics/gg.jpg)
---

**🔥🔥🔥 Baboom ! Un beau NextCloud.**

Naviguez un peu, faites vous plais', vous avez votre propre DropBox n_n

![Well Done](./pics/well_done.jpg)