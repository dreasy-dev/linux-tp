# Partie 2 : FTP

- [Partie 2 : FTP](#partie-2--ftp)
- [I. Intro](#i-intro)
- [II. Setup du serveur FTP](#ii-setup-du-serveur-ftp)
  - [1. Installation du serveur](#1-installation-du-serveur)
  - [2. Lancement du service FTP](#2-lancement-du-service-ftp)
  - [3. Etude du service FTP](#3-etude-du-service-ftp)
  - [4. Modification de la configuration du serveur](#4-modification-de-la-configuration-du-serveur)

# I. Intro

> *FTP* c'est pour *File Transfer Protocol*.

***FTP* est un protocole qui permet d'envoyer simplement des fichiers sur un serveur à travers le réseau.**

*FTP* repose un principe de client/serveur :

- le *serveur*
  - est installé sur une machine par l'admin
  - écoute sur le port 21/TCP par convention
- le *client*
  - connaît l'IP du *serveur*
  - se connecte au *serveur* à l'aide d'un programme appelé "*client FTP*"

Dans la vie réelle, *FTP* est souvent utilisé pour échanger des fichiers avec un serveur de façon sécurisée. En vrai ça commence à devenir oldschool *FTP*, mais c'est un truc très basique et toujours très utilisé.

> Si vous louez un serveur en ligne, on vous donnera parfois un accès *FTP* pour y déposer des fichiers.

# II. Setup du serveur FTP

Toujours la même routine :

- **1. installation de paquet**
  - avec le gestionnaire de paquet de l'OS
- **2. configuration** dans un fichier de configuration
  - avec un éditeur de texte
  - les fichiers de conf sont de simples fichiers texte
- **3. lancement du service**
  - avec une commande `systemctl start <NOM_SERVICE>`

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

## 1. Installation du serveur

🌞 **Installer le paquet `vsftpd`**

---

Note : une fois que le paquet est installé, plusieurs nouvelles choses sont dispos sur la machine. Notamment :

- un service `vsftpd`
- un fichier de configuration `/etc/vsftpd.conf`

> Le paquet s'appelle `vsftpd` pour *Very Secure FTP Daemon*. A l'apogée de l'utilisation de FTP, il n'était pas réputé pour être un protocole très sécurisé. Aujourd'hui, avec des outils comme `vsftpd`, c'est bien mieux qu'à l'époque.

## 2. Lancement du service FTP

🌞 **Lancer le service `vsftpd`**

- avec une commande `systemctl start`
- vérifier que le service est actuellement actif avec une commande `systemctl status`
```bash
● vsftpd.service - vsftpd FTP server                                                                                         Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)                                    Active: active (running)

```

> Vous pouvez aussi faire en sorte que le service FTP se lance automatiquement au démarrage avec la commande `systemctl enable vsftpd`.

## 3. Etude du service FTP

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du service
  - avec une commande `systemctl status`
- afficher le/les processus liés au service `vsftpd`
  - avec une commande `ps`
  ```bash
      1     598     598     598 ?             -1 Ss       0   0:00 /usr/sbin/vsftpd /etc/vsftpd.conf   
    ```
  - isolez uniquement la/les ligne(s) intéressante(s) pour le rendu de TP
- afficher le port utilisé par le service `vsftpd`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)
  ```bash
  tcp     LISTEN   0        32                                                   *:21                           *:*        ino:18528 sk:100c cgroup:/system.slice/vsftpd.service v6only:0 <->  
  ```
- afficher les logs du service `vsftpd`
  - avec une commande `journalctl`
  ```bash
  nov. 05 21:27:21 dreasy-VirtualBox systemd[1]: vsftpd.service: Deactivated  successfully.  
  ```
  - en consultant un fichier dans `/var/log/`
  ```bash
  dreasy@dreasy$ vim /var/log/journal/'files.name'
  ```
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un *client FTP*
  - les navigateurs Web, ils font ça maintenant
  - demandez moi si vous êtes perdus
- essayez d'uploader et de télécharger un fichier
  - montrez moi à l'aide d'une commande la ligne de log pour l'upload, et la ligne de log pour le download
-
- vérifier que l'upload fonctionne
  - une fois un fichier upload, vérifiez avec un `ls` sur la machine Linux que le fichier a bien été uploadé


🌞 **Visualiser les logs**

- mettez en évidence une ligne de log pour un download
```
Fri Nov  5 22:08:38 2021 [pid 1853] [dreasy] OK LOGIN: Client "::ffff:192.168.56.1"                                     Fri Nov  5 22:09:06 2021 [pid 1855] [dreasy] OK DOWNLOAD: Client "::ffff:192.168.56.1", "/home/dreasy/Templates/Plain Text.txt", 0.00Kbyte/sec 
```
- mettez en évidence une ligne de log pour un upload

## 4. Modification de la configuration du serveur

Pour modifier comment un service se comporte il faut modifier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/vsftpd.conf`
- effectuez les modifications suivantes :
  - changer le port où écoute `vstfpd`
    - peu importe lequel, il doit être compris entre 1025 et 65536
        
          # Make sure PORT transfer connections originate from port 1081 (ftp-data).
          connect_from_port_1081=YES   
          
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
- pour les deux modifications, prouver à l'aide d'une commande qu'elles ont bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client FTP*

  ***Filezila***
- re-tester l'upload et le download
      
    
      Client a jour up and download setup

