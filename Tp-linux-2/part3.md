# Partie 3 : Création de votre propre service

- [Partie 3 : Création de votre propre service](#partie-3--création-de-votre-propre-service)
- [I. Intro](#i-intro)
- [II. Jouer avec netcat](#ii-jouer-avec-netcat)
- [III. Un service basé sur netcat](#iii-un-service-basé-sur-netcat)
  - [1. Créer le service](#1-créer-le-service)
  - [2. Test test et retest](#2-test-test-et-retest)

# I. Intro


# II. Jouer avec netcat


🌞 **Donnez les deux commandes pour établir ce petit chat avec `netcat`**

- la commande tapée sur la VM

            apt-get install netcat
- la commande tapée sur votre PC

🌞 **Utiliser `netcat` pour stocker les données échangées dans un fichier**

- utiliser le caractère `>` et/ou `>>` sur la ligne de commande `netcat` de la VM
- cela permettra de stocker les données échangées dans un fichier
- plutôt que de les afficher dans le terminal
- parce quuueeee pourquoi pas ! Ca permet de faire d'autres trucs avec

# III. Un service basé sur netcat

**Pour créer un service sous Linux, il suffit de créer un simple fichier texte.**

Ce fichier texte :

- a une syntaxe particulière
- doit se trouver dans un dossier spécifique

Pour essayer de voir un peu la syntaxe, vous pouvez utilisez la commande `systemctl cat` sur un service existant. Par exemple `systemctl cat sshd`.

DON'T PANIC pour votre premier service j'vais vous tenir la main.

La commande que lancera votre service sera un `nc -l` : vous allez donc créer un petit chat sous forme de service ! Ou presque hehe.

## 1. Créer le service

🌞 **Créer un nouveau service**

- créer le fichier `/etc/systemd/system/chat_tp2.service`
- définissez des permissions identiques à celles des aux autres fichiers du même type qui l'entourent
- déposez-y le contenu suivant :

```bash
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=<NETCAT_COMMAND>

[Install]
WantedBy=multi-user.target
```

Vous devrez remplacer `<NETCAT_COMMAND>` par une commande `nc` de votre choix :

- `nc` doit écouter, listen (`-l`)
- et vous devez préciser le chemin absolu vers cette commande `nc`
  - vous pouvez taper la commande `which nc` pour connaître le dossier où se trouve `nc` (son chemin absolu)

**Il faudra exécuter la commande `sudo systemctl daemon-reload` à chaque fois que vous modifiez un fichier `.service`.**

## 2. Test test et retest

🌞 **Tester le nouveau service**

- depuis la VM
  - démarrer le nouveau service avec une commande `systemctl start`
  - vérifier qu'il est correctement lancé avec une commande  `systemctl status`
  - vérifier avec une comande `ss -l` qu'il écoute bien derrière le port que vous avez choisi
- tester depuis votre PC que vous pouvez vous y connecter
- pour visualiser les messages envoyés par le client, il va falloir regarder les logs de votre service, sur la VM :

```bash
# Voir l'état du service, et les derniers logs
$ systemctl status chat_tp2

# Voir tous les logs du service
$ journalctl -xe -u chat_tp2

# Suivre en temps réel l'arrivée de nouveaux logs
# -f comme follow :)
$ journalctl -xe -u chat_tp2 -f
```
