# Définition des variables
file_dir="/srv/yt/urls"
downloads_dir="/srv/yt/downloads"
logs_dir="/var/log/yt"
date=$(date "+%D %T")

# Vérification que le doss de downloaded existe
if [ ! -d $downloads_dir ]
then
        echo "Le dossier de téléchargements n'existe pas."
        exit
fi

# Vérification que le doss de logs existe
if [ ! -d $logs_dir ]
then
        echo "Le dossier de logs n'existe pas."
        exit
fi

file_content=$(cat file_dir)
if [ -z "${file_content}" ]
then
        echo "$file_content" | while read url
        do
                
                title=$(youtube-dl -e "${url}")
                url=$(echo "$url")
                echo "${file_content}" | grep -v "${url}" > "${file_dir}"

                # Vérification que le nom de la vidéo est dispo puis téléchargement
                if [ -d "/srv/yt/downloads/${title}" ]
                then
                        echo "Une vidéo avec le même titre a déjà été téléchargée.\nAvant de télécharger, vous devez donc supprimer le dossier : ${title}"
                        exit
                fi
                mkdir "/srv/yt/downloads/${title}"
                youtube-dl -o "/srv/yt/downloads/$title/$title.mp4" --format mp4 $url > /dev/null
                youtube-dl --get-description $1 > "/srv/yt/downloads/$title/description"
                echo "Video ${url} was downloaded"
                echo "File path : /srv/yt/downloads/${title}/${title}.mp4"

                # logs
                echo "[${date}] Video ${url} was downloaded. File path : $path" >> /var/log/yt/download.log
        done
else
        exit
fi