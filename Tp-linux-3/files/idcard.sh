name=$(cat /etc/hostname)    
os=$(cat /etc/os-release | grep -w "NAME" | cut -d'"' -f2)                                                              
kernel_version=$(uname -r)                      
ip=$(ip a | grep inet | grep enp0s8 | tr -s '[:space:]' | cut -d' '-f3)    
ram=$(free -h --giga | grep Mem: | tr -s '[:space:]') 
storage=$(df -h / | grep / | tr -s '[:space:]' | cut -d' ' -f4)  
random_cat=$(curl -s https://api.thecatapi.com/v1/images/search | cut -c 2- | rev | cut -c 2- | rev | jq -r ".url")   
echo "Machine name : $name"                     
echo "OS : $os and kernel version is $kernel_version"                           
echo "IP : $ip"                                              
echo "RAM : $(echo $ram | cut -d' ' -f4) / $(echo $ram | cut -d' ' -f2)"  
echo "Disque : $storage space left"             
echo "Top 5 processes by RAM usage :
    - En cours ...
    - En cours ...
    - En cours ...
    - En cours ..."                                                                                                                                                                                                    
echo "Listening ports:
 
- En cours ..."                                                                                                       
echo "Here's your random cat : $random_cat"     