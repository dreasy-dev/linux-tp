# TP3 : A little script

- [TP 3 : A little script](#tp-3--a-little-script)
- [I. Script carte d'identité](#i-script-carte-didentité)
  - [Visualiser le script (V1)](./files/idcard.sh)
- [II. Script youtube-dl](#ii-script-youtube-dl)
  - [Visualiser le script (V1)](./files/yt.sh)
- [III. MAKE IT A SERVICE](#iii-make-it-a-service)
  - [Visualiser le fichier service](./files/yt.service)
  - [Visualiser le script (V2)](./files/yt-v2.sh)


## I. Script carte d'identité

Exemple d'utilisation :

        Machine name : dreasy-VirtualBox                                                                                        OS : Ubuntu and kernel version is 5.13.0-21-generic                                                                     IP : 192.168.56.111/24                                                                                                  RAM : 6,5G / 7,4G                                                                                                       Disque : 24G space left                                                                                                 Top 5 processes by RAM usage :
                 
                 
                 
                 
                 
                 
                                                                             
              Listening ports :       
                  - 53 ()
                  - 24 ()
                  - 630 ()
                  - 24 ()
                  - 630 ()
                                                                                              Here's your random cat : https://cdn2.thecatapi.com/images/VsxceZVop.jpg 

#### ➜ [**Visualiser le script**](./files/idcard.sh)

## II. Script youtube-dl

Exemple d'utilisation :

```bash
dreasy@dreasy$ sudo sh /srv/yt/youtube.sh https://www.youtube.com/watch?v=HjXxvooa-0g
Video https://www.youtube.com/watch?v=HjXxvooa-0g was downloaded
File path : /srv/yt/downloads/Joyeux Anniversaire - Patrick Sébastien

```

#### ➜ [**Visualiser le script**](./files/youtube.sh)

## III. MAKE IT A SERVICE
- Commande systemctl status yt

            dreasy@dreasy:$ sudo systemctl status yt
      ● yt.service - Telechargement de videos YouTube
          Loaded: loaded (/etc/systemd/system/yt.service; disabled; vendor preset: enabled)
          Active: activating (auto-restart) since Mon 2021-11-22 23:58:30 CET; 6s ago
          Process: 3088 ExecStart=/usr/bin/sudo sh /srv/yt/yt-v2.sh 
        Main PID: 3088 

      nov. 22 23:58:30 dreasy.tp3.linux systemd[1]: yt.service: Succeeded.

- Extrait de `journalctl -xe -u yt`


      dreasy@dreasy:$ journalctl -xe -u yt
      
      -- The job identifier is 7158 and the job result is done.
      nov. 22 23:59:51 node1.tp3.linux systemd[1]: Started Telechargement de videos YouTube.
      -- Subject: A start job for unit yt.service has finished successfully
      -- Defined-By: systemd
      -- Support: http://www.ubuntu.com/support
      --
      -- A start job for unit yt.service has finished successfully.
      --
      -- The job identifier is 7158.
      nov. 22 23:59:51 node1.tp3.linux sudo[3178]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/sh /srv/yt/yt-v2.sh
      nov. 22 23:59:51 node1.tp3.linux sudo[3178]: pam_unix(sudo:session): session opened for user root by (uid=0)



- Commande pour démarrer automatiquement le service lorsque la machine démarre


      dreasy@dreasy:$ sudo systemctl enable yt
      Created symlink /etc/systemd/system/multi-user.target.wants/yt.service → /etc/systemd/system/yt.service.


#### ➜ [**Visualiser le fichier service**](./files/youtube.service)
#### ➜ [**Visualiser le script**](./files/youtube-v2.sh)                                                                      