# Partie 4 : Scripts de sauvegarde

Ok la dernière étape : faire en sorte que, à intervalles réguliers, on sauvegarde les données de NextCloud.

L'idée :

- un script tourne sur `web.tp6.linux`
  - il sauvegarde le dossier qui contient les fichiers de NextCloud
- un script tourne sur `db.tp6.linux`
  - il extrait le contenu de la base sous la forme d'un fichier
  - il sauvegarde ce fichier

> Par "sauvegarder" on entend : créer une archive compressée, la nommer correctement, la déplacer dans le bon dossier, et générer une ligne de log.

## I. Sauvegarde Web

🌞 **Ecrire un script qui sauvegarde les données de NextCloud**

- le script crée un fichier `.tar.gz` qui contient tout le dossier de NextCloud
- le fichier doit être nommé `nextcloud_yymmdd_hhmmss.tar.gz`
- il doit être stocké dans le répertoire de sauvegarde : `/srv/backup/`
- le script génère une ligne de log à chaque backup effectuée
  - message de log : `[yy/mm/dd hh:mm:ss] Backup /srv/backup/<NAME> created successfully.`
  - fichier de log : `/var/log/backup/backup.log`
- le script affiche une ligne dans le terminal à chaque backup effectuée
  - message affiché : `Backup /srv/backup/<NAME> created successfully.`
```bash
#!/bin/bash
#!Author Dreasy
filename="nextcloud_$(date +"%y%m%d")_$(date +"%H%M%S").tar.gz"
cd /var/www && tar -czf "/srv/backup/${filename}" nextcloud
echo "Backup /srv/backup/${filename} created successfully."
echo "[$(date +"%y:%m:%d") $(date +"%H:%M:%S")] Backup /srv/backup/${filename} created successfully." >> /var/log/backup/backup.log

```
> Vu que l'archive est stockée dans `/srv/backup/`, en fait bah elle est stockée sur la machine `backup.tp6.linux`. Hé ui, c bo no ?

🌞 **Créer un service**

- créer un service `backup.service` qui exécute votre script
- ainsi, quand on lance le service avec `sudo systemctl start backup`, une backup est déclenchée

**NB : vous DEVEZ ajoutez la ligne `Type=oneshot` en dessous de la ligne `ExecStart=` dans votre service pour que tout fonctionne correctement avec votre script.**

```bash
[dreasy@web ~]$ cat /etc/systemd/system/backup.service
[Unit]
Description=Auto Backup

[Service]
ExecStart=/usr/bin/bash /etc/systemd/system/backup.sh
Type=oneshot

[Install]
WantedBy=multi-user.target

```bash

[dreasy@web ~]$ systemctl start backup.service
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'backup.service'.
Authenticating as: Dreasy (dreasy)
Password:
==== AUTHENTICATION COMPLETE ====
[dreasy@web ~]$ systemctl status backup.service
● backup.service - Auto Backup
Loaded: loaded (/etc/systemd/system/backup.service; enabled; vendor preset: disabled)
Active: inactive (dead) since Tue 2021-12-07 17:33:41 CET; 2s ago
Process: 5901 ExecStart=/usr/bin/bash /etc/systemd/system/backup.sh (code=exited, status=0/SUCCESS)
Main PID: 5901 (code=exited, status=0/SUCCESS)                                                                                                                                  
```


🌞 **Vérifier que vous êtes capables de restaurer les données**

- en extrayant les données
- et en les remettant à leur place
petit problemes de permissions 
```bash
[dreasy@web ~]$ tar -xf nextcloud_211207_173259.tar.gz
[dreasy@web ~]$ ls
nextcloud  nextcloud_211207_173259.tar.gz
[dreasy@web ~]$ sudo -g apache mv nextcloud_211207_173259.tar.gz /var/www/
[sudo] password for dreasy:
[dreasy@web ~]$ ^C
[dreasy@web ~]$ sudo -g apache mv nextcloud /var/www/
[sudo] password for dreasy:
Sorry, user dreasy is not allowed to execute '/bin/mv nextcloud /var/www/' as dreasy:apache on web.tp5.linux.  
```

🌞 **Créer un *timer***

- un *timer* c'est un fichier qui permet d'exécuter un service à intervalles réguliers
- créez un *timer* qui exécute le service `backup` toutes les heures

Pour cela, créer le fichier `/etc/systemd/system/backup.timer`.

> Notez qu'il est dans le même dossier que le service, et qu'il porte le même nom, mais pas la même extension.

Contenu du fichier `/etc/systemd/system/backup.timer` :

```bash
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```

Activez maintenant le *timer* avec :

```bash

# démarrage immédiat du timer
$ sudo systemctl start backup.timer

# activation automatique du timer au boot de la machine
$ sudo systemctl enable backup.timer
```

Enfin, on vérifie que le *timer* a été pris en compte, et on affiche l'heure de sa prochaine exécution :

```bash
[dreasy@web /]$ sudo systemctl list-timers                                                                                            NEXT                         LEFT       LAST                         PASSED       UNIT                         ACTIVATES              Tue 2021-12-07 17:58:18 CET  13min left n/a                          n/a          systemd-tmpfiles-clean.timer systemd-tmpfiles-clean>Tue 2021-12-07 18:00:00 CET  15min left n/a                          n/a          backup.timer                 backup.service  

```

## II. Sauvegarde base de données

🌞 **Ecrire un script qui sauvegarde les données de la base de données MariaDB**

- il existe une commande : `mysqldump` qui permet de récupérer les données d'une base SQL sous forme d'un fichier
  - le script utilise cette commande pour récup toutes les données de la base `nextcloud` dans MariaDB
  - on dit que le script "dump" la base `nextcloud`
  - petit point sur la commande `mysqldump` plus bas
- le script crée un fichier `.tar.gz` qui contient le fichier issu du `mysqldump`
- le fichier doit être nommé `nextcloud_db_yymmdd_hhmmss.tar.gz`
- il doit être stocké dans le répertoire de sauvegarde : `/srv/backup/`
- le script génère une ligne de log à chaque backup effectuée
  - message de log : `[yy/mm/dd hh:mm:ss] Backup /srv/backup/<NAME> created successfully.`
  - fichier de log : `/var/log/backup/backup_db.log`
- le script affiche une ligne dans le terminal à chaque backup effectuée
  - message affiché : `Backup /srv/backup/<NAME> created successfully.`

➜ **La commande `mysqldump`** fonctionne quasiment exactement pareil que la commande `mysql` dont vous vous êtes déjà servi.  
C'est un simple client SQL aussi.

La différence : `mysql` permet d'avoir un shell interactif, tandis que `mysqldump` se contente de lire toutes les données de la base et de les afficher dans le terminal.

Ainsi, si on est capable de se connecter à une base avec :

```bash
$ mysql -h 192.168.1.1 -p -u super_user super_base
```

Alors on pourra dump la base dans un fichier `super_dump.sql` avec :

```bash
$ mysqldump -h 192.168.1.1 -p -u super_user super_base > super_dump.sql
```

---

🌞 **Créer un service**

- créer un service `backup_db.service` qui exécute votre script
- ainsi, quand on lance le service, une backup de la base de données est déclenchée
```bash

[dreasy@db /]$ sudo cat /etc/systemd/system/backup.sh
#!/bin/bash
filename="nextcloud_db_$(date +"%y%m%d")_$(date +"%H%M%S").tar.gz"
mysqldump -u root -proot nextcloud > /home/dreasy/nextcloud.sql
cd /home/dreasy && tar -czf "/srv/backup/${filename}" nextcloud.sql
rm nextcloud.sql
echo "Backup /srv/backup/${filename} created successfully."
echo "[$(date +"%y:%m:%d") $(date +"%H:%M:%S")] Backup /srv/backup/${filename} created successfully." >> /var/log/backup/backup.log
```
```bash
[dreasy@db /]$ sudo systemctl status backup_database.service
● backup_database.service - Auto Backup
Loaded: loaded (/etc/systemd/system/backup_database.service; enabled; vendor preset: disabled)
Active: inactive (dead) since Tue 2021-12-07 21:11:56 CET; 10s ago
Process: 28535 ExecStart=/usr/bin/bash /etc/systemd/system/backup.sh (code=exited, status=0/SUCCESS)
Main PID: 28535 (code=exited, status=0/SUCCESS)                                                                                                                            
```
**NB : vous DEVEZ ajoutez la ligne `Type=oneshot` en dessous de la ligne `ExecStart=` dans votre service pour que tout fonctionne correctement avec votre script.**

🌞 **Créer un `timer`**

- il exécute le service `backup_db.service` toutes les heures


```pm
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup_database.service

[Timer]
Unit=backup_database.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
                                                                                                                            
```
## Conclusion

Dans ce TP, plusieurs notions abordées :

- partitionnement avec LVM
- gestion de partitions au sens large
- partage de fichiers avec NFS
- scripting

Et à la fin ? Toutes les données de notre cloud perso sont sauvegardéééééééééééééééées. Le feu.

![Backups everywhere](./pics/backups_everywhere.jpg)