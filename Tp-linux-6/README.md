# TP6 : Stockage et sauvegarde

Dans ce TP on va aborder plusieurs thèmes autour du stockage :

- gestion de périphériques de stockage
- gestion de partitions et filesystems
- serveur NFS
- sauvegarde et planification de tâche

Histoire de se rapprocher d'un cas réel, on va réutiliser les machines du [TP5](../5/README.md).  
En effet, le TP5 correspondait à un service Web typique : un serveur Web et sa base de données.

**Le principe du TP est simple : mettre en place des sauvegardes automatisées pour les données de NextCloud.**

Effectuer une sauvegarde c'est le fait d'effectuer une copie de certaines données à un instant T, dans le but de pouvoir les restaurer plus tard.  
Cela permet de se protéger contre la perte de données.

Dans ce TP, on va ajouter une troisième machine : `backup.tp6.linux`. Son rôle sera d'héberger les sauvegardes.

> Concrètement, une sauvegarde, c'est juste une archive compressée.

![Gotta be kitten me](./pics/gotta_be_kitten_me.jpg)

---

Ce qu'on aimerait pour nos sauvegardes :

➜ **on utilise un espace dédié au stockage des sauvegardes**

- dans un disque dur particulier, dédié à ça
- sur une machine particulière, dédiée à ça
- pour nous ce sera un disque dédié sur la machine `backup.tp6.linux`
- pkoa ?
  - ça augmente le niveau de sécu de nos données que de dédier des machines à ça
  - qui dit volume dédié, dit possibilité de mettre en place du [RAID](https://fr.wikipedia.org/wiki/RAID_(informatique)) par exemple aussi

> On prépare ça dans la [Partie 1](./part1.md).

➜ **les sauvegardes doivent circuler à travers le réseau**

- bah ouais, les données à backup sont sur les machines `web.tp6.linux` et `db.tp6.linux`
- donc va falloir les envoyer sur `backup.tp6.linux` à travers le réseau
- dans notre cas on va utiliser NFS
  - c'est un protocole très simple permettant d'échanger des fichiers entre deux machines
  - la machine `backup.tp6.linux` sera le serveur NFS
  - les machines `web.tp6.linux` et `db.tp6.linux` seront des clients NFS

> On setup le serveur NFS en [Partie 2](./part2.md) et les clients en [Partie 3](./part3.md).

➜ **les sauvegardes doivent être organisées**

- nommage
  - les fichiers sont bien nommés
  - dans des dossiers spécifiques
- les sauvegardes sont exécutées à intervalles réguliers
- elles produisent des logs

> On va faire tout ça avec des scripts `bash`, c'est l'objet de la [Partie 4](./part4.md)

# Sommaire

Dans ce document :

- [TP6 : Stockage et sauvegarde](#tp6--stockage-et-sauvegarde)
- [Sommaire](#sommaire)
- [Partie 0 : Prérequis](#partie-0--prérequis)
- [Go next !](#go-next-)

Les différentes parties du TP :

- [Partie 1 : Préparation de la machine `backup.tp6.linux`](./part1.md)
- [Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`](./part2.md)
- [Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`](./part3.md)
- [Partie 4 : Scripts de sauvegarde](./part4.md)

# Partie 0 : Prérequis

Pour ce TP, on va donc avoir besoin de trois machines virtuelles 🖥️.

> Pour nous faciliter la vie, j'ai pas changé les IPs des machines par rapport au [TP5](../5/README.md).

| Machine            | IP             | Service                              |
|--------------------|----------------|--------------------------------------|
| `web.tp6.linux`    | `10.5.1.11/24` | Serveur Web : Apache                 |
| `db.tp6.linux`     | `10.5.1.12/24` | Serveur de base de données : MariaDB |
| `backup.tp6.linux` | `10.5.1.13/24` | Serveur de sauvegarde                |

Vous déroulerez la **📝checklist📝** suivante, sur les trois machines :

- [x] la VM a un accès internet
  - `ping 1.1.1.1` fonctionnel
  - résolution de nom fonctionnelle : `ping ynov.com` fonctionnel
- [x] SELinux est désactivé
  - la commande `sestatus` doit retourner : `Current Mode: permissive`
- [x] vous avez une connexion SSH fonctionnelle vers la VM
  - avec un échange de clé
- [x] la machine est nommée
  - fichier `/etc/hostname`
  - commande `hostname`

> La checklist ne doit pas figurer dans le rendu, elle doit simplement être effectuée sur toutes les machines. Tout manquement à la checklist vous vaudra des points en moins.

# Go next !

Une fois les prérequis en place, vous pouvez enchaîner sur les différentes parties du TP :

- [Partie 1 : Préparation de la machine `backup.tp6.linux`](./part1.md)
- [Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`](./part2.md)
- [Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`](./part3.md)
- [Partie 4 : Scripts de sauvegarde](./part4.md)